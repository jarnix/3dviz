import ddf.minim.analysis.*;
import ddf.minim.*;
 
Minim minim;
AudioPlayer song;
FFT fft;
 
// the number of bands per octave
int bandsPerOctave = 4;
 
// the spacing between bars
int barSpacing = 3;
 
void setup()
{
  size(512, 200);
 
  minim = new Minim(this);
 
  song = minim.loadFile("../test.mp3", 1024);
  song.loop();
 
  fft = new FFT(song.bufferSize(), song.sampleRate());
 
  // calculate averages based on a miminum octave width of 22 Hz
  // split each octave into a number of bands
  fft.logAverages(22, bandsPerOctave);
 
  rectMode(CORNERS);
}
 
void draw()
{
  background(0);
  stroke(255);
 
  // perform a forward FFT on the samples in song's mix buffer
  fft.forward(song.mix);
 
  // avgWidth() returns the number of frequency bands each average represents
  // we'll use it as the width of our rectangles
  int w = int(width/fft.avgSize());
 
  for(int i = 0; i < fft.avgSize(); i++)
  {
    // get the amplitude of the frequency band
    float amplitude = fft.getAvg(i);
 
    // convert the amplitude to a DB value. 
    // this means values will range roughly from 0 for the loudest
    // bands to some negative value.
    float bandDB = 20 * log(2 * amplitude / fft.timeSize());
    // so then we want to map our DB value to the height of the window
    // given some reasonable range
    float bandHeight = map(bandDB, 0, -150, 0, height);
 
    // draw a rectangle for the band
    rect(i*w, height, i*w + w - barSpacing, bandHeight);
  }
}

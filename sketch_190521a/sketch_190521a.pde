import ddf.minim.*;
import ddf.minim.analysis.*;

Minim minim;
AudioPlayer player;
FFT         fft;
float[] spectrum;
float decay = 0.95;
float heightScale = 0.5;

int cols, rows;
int scl = 10;
int w = 1000;
int h = 800;
int colorrgb = 0;
int row = 0;
int frame = 0;

float flying = 0;

float[][] terrain;
float[] currentTerrain;
float[][] newTerrain; 

void setup() {
  
  size(1000, 800, P3D);
  
  minim = new Minim(this);
  player = minim.loadFile("../8brutalmm.mp3");
  player.setGain(-10);
  player.loop();  
  fft = new FFT(player.bufferSize(), player.sampleRate());
  spectrum = new float[fft.specSize()];
  cols = w / scl;
  rows = h / scl;
  currentTerrain = new float[cols];
  terrain = new float[cols][rows];
  newTerrain = new float[cols][rows];
  frame = 0;
  for (int y = 0; y < rows; y++) {
    for (int x = 0; x < cols; x++) {
      terrain[x][y] = 0;
      newTerrain[x][y] = 0;
    }
  }
}

void draw() {

  fft.forward(player.mix);

  frame++;

  int xprime;

  for (int x = 0; x < cols; x++) {

    xprime = floor(x * fft.specSize() / cols);

    spectrum[xprime] += fft.getBand(xprime);
    spectrum[xprime] = spectrum[xprime] <= 0 ? 0 : log(spectrum[xprime]) * heightScale;

    currentTerrain[x] = map(spectrum[xprime], 0, 1, -100, 100);
    println(currentTerrain[x]);
  }

  if (frame % 3 == 0 || frame == 1) {
    for (int y = 1; y < rows; y++) {
      for (int x = 0; x < cols; x++) {
        newTerrain[x][y] = decay * terrain[x][y-1];
      }
    }

    for (int x = 0; x < cols; x++) {
      newTerrain[x][0] = currentTerrain[x];
      newTerrain[x][1] = currentTerrain[x];
    }
    for (int y = 0; y < rows; y++) {
      for (int x = 0; x < cols; x++) {
        terrain[x][y] = newTerrain[x][y];
      }
    }
    background(0);
    stroke(255, 100, 50);
    noFill();

    // translate(width/2, height/2);
    // rotateX(PI/4);
    //translate(-width/2, -height/2);
    
    camera(width/2.0, -height/2.0, (height/2.0) / tan(PI*30.0 / 180.0),
           width/2.0, -height/2.0, 0,
           0, 1, 0);
    
    
    for (int y = 0; y < rows-1; y++) {
     
      beginShape(TRIANGLE_STRIP);
      //stroke(255 / ((rows - y) * 0.1), 255 / ((rows - y) * 0.1), 50 / ((rows - y) * 0.1));      
      for (int x = 0; x < cols; x++) {
        /*
        if(terrain[x][y] > -300 && terrain[x][y] <= -120) {
          stroke(255, 255, 255);
        }
        else {
          stroke(255 / ((rows - y) * 0.1), 255 / ((rows - y) * 0.1), 50 / ((rows - y) * 0.1));    
        }
        */
        vertex(x*scl, y*scl, terrain[x][y]);
        vertex(x*scl, (y+1)*scl, terrain[x][y+1]);
      }
      endShape();
    }
    
  }
  
}

void stop()
{
  // always close Minim audio classes when you finish with them
  player.close();
  minim.stop();

  super.stop();
}

import ddf.minim.*;
import ddf.minim.analysis.*;
 
 
Minim       minim;
AudioPlayer song;
FFT         fft;
 
float[][] lineArray;        //holds the FFT data
float[][] tempLineArray;    //holds the temp FFT data to shift FFT data down the screen
 
int cols = 600;     //width of lines across
int rows = 62;      //number of lines down
int xoffset = 150;  //X offset
int yoffset = 12;   //distance between lines
int yshift = 100;   //Y shift from top of screen
 
 
void setup(){
size(900,900);
background(0);
smooth();
 
    minim = new Minim(this);
    song = minim.loadFile("song.mp3",1024);
    song.play();
    fft = new FFT( song.bufferSize(), song.sampleRate() );
 
      lineArray = new float[cols][rows];
      tempLineArray = new float[cols][rows];
}
 
void draw(){
    background(0);
    stroke(180);
    strokeWeight(2);
    fill(0);
    sound();
    displayLines();
}
 
 
void sound(){                   //writes current band to 0 position of lineArray
 
for(int i = 0; i < cols; i++)
  {
    lineArray[i][0]=song.left.get(i)*30;
  }
 
}
 
void displayLines(){
 
for(int x = 0; x<rows-1;x++){               //shifts all lineArray data to tempLineArray down one then writes it back to lineArray
    for(int i = 0; i < cols;i++){
    tempLineArray[i][x+1]=lineArray[i][x];
    lineArray[i][x]=tempLineArray[i][x];
 
    }
}
 
 for(int y =1;y<rows-1;y++){
 
    beginShape();
    vertex(xoffset,height);
 
    for(int x=0;x<cols;x++){
 
      vertex(xoffset+x,lineArray[x][y]+y*yoffset+yshift);
 
    }
 
    vertex(width-xoffset,height);
    endShape(CLOSE);
  }
  noStroke();
  rect(0,0,xoffset+5,height);
  rect(width-xoffset-5,0,width,height);
    fill(180);
    textFont(createFont("arial.ttf",75));
    text("TALKING HEADS", xoffset, yshift-15);
    fill(255);
    textFont(createFont("arialbd.ttf",49));
    text("NOTHING BUT FLOWERS", xoffset, height-10);  
 
}
 
 
void mousePressed(){
    noLoop();
}
 
 
void stop(){
  minim.stop();
  super.stop();
  song.close();
}

import ddf.minim.analysis.*;
import ddf.minim.*;
import processing.dxf.*;

//parameters
int m = 4; // logavg 1 controls distribution of spectrum
int n = 7; // logavg 2 controls amplitude of spectrum
int ptDensity = 210; // density of circle around y-axis
float beta = 0; //angle
int idx = 0; //index
float r; //radius
float initialScale = 1.1;
float finalScale = 0.25;
float flashScale = 3;
float sc = initialScale; // current object scale

// sound
Minim       minim;
FFT         fft;
AudioInput in;
AudioPlayer thinking;

// sphere
PVector [][] coords;

// setup
int waveSize = 0;
boolean waveHit = true;
int waveFade = 255;
float millisecs;
float previousMillisecs = 0;
float rotation = 0;
float beatsMillisecs = 0;
float BPM = 128.0;
int songDurationSeconds = 1;
AudioMetaData meta;

//===================================================================

void setup() {

  size(1920, 1080, P3D);
    
  frameRate(30);

  // millisecs per 1/4 beat
  beatsMillisecs = 4 * ceil(1000 / (BPM / 60));

  // sound analyzer
  minim = new Minim(this);
  thinking = minim.loadFile("../../test.mp3", 1024);
  meta = thinking.getMetaData();
  songDurationSeconds = meta.length() / 1000;
  println(songDurationSeconds);
  thinking.play();
  fft = new FFT(thinking.bufferSize(), thinking.sampleRate());
  fft.logAverages(m, n); 

  // sphere calculations
  r = width/100; // radius
  beta = TWO_PI/ptDensity; // y axis rotation
  coords = new PVector [ptDensity+1][fft.avgSize()];
  float x, y, z;
  for (int i = 0; i<coords.length; i++) {
    float u = (i*TWO_PI/ptDensity);
    for (int j = 0; j<coords[i].length; j++) {
      float v = (PI/coords[i].length)*j-PI/2;
      x=r* sin(u)* cos(v); 
      y=r * cos(u) * cos(v); 
      z= r * sin(v);
      coords[i][j] = new PVector(x, y, z);
    }
  }
}

void draw() {
  lights();
  background(0);
  
  pushMatrix();

  translate(width/2, height/2, width/3);

  sc=initialScale + (finalScale - initialScale) * millis() / (songDurationSeconds*1000);

  // setup view 
  millisecs = millis() /*- beatsMillisecs*/;
    
  if(millisecs % beatsMillisecs <= 100) {
    if(millisecs - previousMillisecs > 500) {
      background(255);
      previousMillisecs = millisecs;
      sc = flashScale;
    }
  }
  else {
     background(0);
  }
  
  // for the song's end
  if(millis() >= songDurationSeconds * 1000) {
    rotation = 0;
    sc = finalScale;
    rotateY(0);
  }
  else {
    rotation = millisecs % beatsMillisecs;
    // rotateY(0.001 * rotation);
  }
  
  if (rotation > beatsMillisecs / 2) {
    // rotateX(7*PI/6);
    rotateX((7+5*((beatsMillisecs-rotation)/beatsMillisecs))*PI/6);
  }
  else {
    rotateX(7*PI/6);
  }
  
  scale(sc);
  
  // call input
  fft.forward(thinking.mix);

  // advance index
  idx = (idx+1)%(ptDensity);

  // calculate current row
  for (int i = 0; i<fft.avgSize(); i++) { //this loop draws the half circle
    float theta = (PI/fft.avgSize())*i-PI/2;
    // change mag
    float add = (coords[idx][i].mag() + (0.05*(r+modFactor(i)*fft.getAvg(i))));
    coords[idx][i].setMag(add);
  }

  // draw sphere
  for (int j = 0; j< (ptDensity); j++) { //loop rotates half circle around y-axis to form sphere
    beginShape(QUAD_STRIP);
    for (int i = 0; i<fft.avgSize(); i++) { //this loop draws the half circle

      noStroke();
      
      stroke(
        map(fft.getAvg(i), 0, fft.avgSize(), 50, 250),
        0,
        100
       );

      fill(
        map(fft.getAvg(i), 0, fft.avgSize(), 100, 256),
        0,
        0);

      vertex(coords[j][i].x, coords[j][i].y, coords[j][i].z);

      if (j == ptDensity-1) {
        vertex(coords[0][i].x, coords[0][i].y, coords[0][i].z);
      } else {
        vertex(coords[j+1][i].x, coords[j+1][i].y, coords[j+1][i].z);
      } // create quadstrip-compatible geometry
    }
    endShape(CLOSE);
  }
  
  popMatrix();

}


//===================================================================

float modFactor(float i) {
  float a = .06;
  float b = .2;
  return a*i+b;
}


//===================================================================
//modification object scale 

void keyTyped() {
  if (key == 'n' || key == 'N') {
    sc -= 0.1;
  } else if (key == 'm' || key == 'M') {
    sc += 0.1;
  }
}

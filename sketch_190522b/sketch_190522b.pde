import ddf.minim.*;
PShader shift;
Minim minim;
AudioPlayer sample;
void setup() {
  size(960, 540, P3D);
  shift = loadShader("shift.glsl");
  background(0);
  frameRate(50);
  smooth();
  minim = new Minim(this);
  sample = minim.loadFile("../test.mp3", 1024);
  sample.play();
}


void draw() {
  background(0);
  stroke(128);
  strokeWeight(1);
  for ( int i=0; i<height/60; i++) {
    line(0, i*60, width, i*60);
  }
  for ( int i=0; i<width/60; i++) {
    line(i*60, 0, i*60, height);
  }
  translate(width/2, height/2);
  scale(1, 2);
  rotate(-PI/4);
  strokeWeight(2);
  stroke(255, 120);
  noFill();
  beginShape();
  float f = 400;
  float left[] = sample.left.toArray();

  for ( int i =0; i<sample.bufferSize() -10; i++) {
        float x1 = left[i];
    float x2 = left[i+10];
    curveVertex( (x1)*f, (x2)*f);
  }
  endShape();

  filter(shift);
}

/*
uniform sampler2D texture;
uniform vec2 texOffset;

varying vec4 vertColor;
varying vec4 vertTexCoord;

void main(void) {
    vec2 mid = vec2(0.5,0.5);
    vec2 coord = (vertTexCoord.st - mid) * 0.9  + mid;
    float d = pow(distance(coord, mid),2)*.1;
    vec2 dir= normalize(coord - mid);
    vec4 col1 = texture2D(texture, coord + dir*d);
    float d2 = pow(distance(coord, mid),2)*.115;
    vec4 col2 = texture2D(texture, coord + dir*d2);

    vec4 c = vec4( col1.r, col2.g, col2.b, 1.0);

    gl_FragColor = c;
}
*/


import ddf.minim.*;
import ddf.minim.analysis.*;

Minim minim;
AudioPlayer song;
FFT fft;

int sampleRate = 44100;
int timeSize = 1024;

void setup() {
  size(500, 500);
  smooth();

  minim = new Minim(this);
  song = minim.loadFile("../test.mp3", 2048);
  song.loop();
  fft = new FFT( song.bufferSize(), song.sampleRate() ); // make a new fft

  // calculate averages based on a miminum octave width of 11 Hz
  // split each octave into 1 bands - this should result in 12 averages
  fft.logAverages(11, 1); // results in 12 averages, each corresponding to an octave, the first spanning 0 to 11 Hz. 
}

void draw() {
  background(0);
  noStroke();

  fft.forward(song.mix); // perform forward FFT on songs mix buffer

  // float bw = fft.getBandWidth(); // returns the width of each frequency band in the spectrum (in Hz).
  // println(bw); // returns 21.5332031 Hz for spectrum [0] & [512]

  for (int i = 0; i < 12; i++) {  // 12 is the number of bands 
  
    int lowFreq;

    if ( i == 0 ) {
      lowFreq = 0;
    } 
    else {
      lowFreq = (int)((sampleRate/2) / (float)Math.pow(2, 12 - i));
    }

    int hiFreq = (int)((sampleRate/2) / (float)Math.pow(2, 11 - i));

    // we're asking for the index of lowFreq & hiFreq
    int lowBound = fft.freqToIndex(lowFreq); // freqToIndex returns the index of the frequency band that contains the requested frequency
    int hiBound = fft.freqToIndex(hiFreq); 

    //println("range " + i + " = " + "Freq: " + lowFreq + " Hz - " + hiFreq + " Hz " + "indexes: " + lowBound + "-" + hiBound);

    // calculate the average amplitude of the frequency band
    float avg = fft.calcAvg(lowBound, hiBound);
    // println(avg);

    if ((lowBound >= 32) && ( hiBound <= 64)) {
      fill(255);
      ellipseMode(CENTER);
      ellipse(250, 250, avg/1, avg/1);
    }
    
    if ((lowBound >= 256) && ( hiBound <= 512)) {
      noFill();
      stroke(255);
      strokeWeight(2);
      ellipseMode(CENTER);
      ellipse(250, 250, avg*20, avg*20);
    }    
    
  }
}

void stop() {  
  song.close(); // always close Minim audio classes when you are finished with them
  minim.stop(); // always stop Minim before exiting
  super.stop(); // this closes the sketch
}
